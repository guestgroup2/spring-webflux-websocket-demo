package com.eddie.replication.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
public class ReplicationServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReplicationServerApplication.class, args);
    }

}
