package com.eddie.replication.server.client;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.FluxSink;

@Slf4j
@Data
public class SocketClient {

    private String streamId;
    private FluxSink<WebSocketMessage> sink;
    private WebSocketSession session;
    private JSONObject userProfile;
    private String docRefNo;

    public SocketClient(FluxSink<WebSocketMessage> sink, WebSocketSession session) {
        this.sink = sink;
        this.session = session;
    }

    public SocketClient(String streamId, FluxSink<WebSocketMessage> sink, WebSocketSession session) {
        this.streamId = streamId;
        this.sink = sink;
        this.session = session;

    }

    public void sendData(String data) {
        sink.next(session.textMessage(data));
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public FluxSink<WebSocketMessage> getSink() {
        return sink;
    }

    public void setSink(FluxSink<WebSocketMessage> sink) {
        this.sink = sink;
    }

    public WebSocketSession getSession() {
        return session;
    }

    public void setSession(WebSocketSession session) {
        this.session = session;
    }

    public JSONObject getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(JSONObject userProfile) {
        this.userProfile = userProfile;
    }

    public String getDocRefNo() {
        return docRefNo;
    }

    public void setDocRefNo(String docRefNo) {
        this.docRefNo = docRefNo;
    }
}
